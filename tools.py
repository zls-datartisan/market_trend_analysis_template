# -*- coding: utf-8 -*-
import re
import json, math
import requests
from datetime import datetime, date, timedelta
import numpy as np
import pandas as pd
import configs
from datas import jungle_scout_data

def str2num(string):
    string = str(string)
    string = string.replace(',','')
    regular_expression_num = '\d+\.?\d*'
    pattern = re.compile(regular_expression_num)
    match = pattern.search(string)
    if match:
        return float(match.group())
    else:
        return float('nan')

def read_data(data_path, header=0):
    converters = {'Price':str2num,
                  'Rank':str2num,
                  'Rating':str2num,
                  'Sales':str2num,
                 'Revenue':str2num,
                 'Reviews':str2num}
    data = pd.read_csv(data_path,
                      converters=converters,
                      header=header,
                      encoding='latin-1')
    return data 

def columns_rollback(data):
    
    """由于列的名字变化，需要回退为旧版本来统一"""
    
    old_columns = ['#', 'Name', 'Brand', 'Price', 'Category', 'Rank', 'Sales', 'Revenue', 'Reviews', 'Rating', 'Seller', 'ASIN', 'Link']

    new_columns = data.columns
    for old_column,new_column in zip(old_columns, new_columns):
        if new_column in old_column:
            data.columns = data.columns.str.replace(new_column,old_column)
    return data

def convert_to_daily_data(raw_data, from_date='2017-01-01', to_date='2017-04-01'):
    """
    keepa时间序列转换
    """
    data = {}

    from_date = datetime.strptime(from_date, '%Y-%m-%d')
    to_date = datetime.strptime(to_date, '%Y-%m-%d')
    delta = timedelta(days=1)
    
    cursor_date = to_date
    
    
    if not raw_data:
        
        while (cursor_date >= from_date) & (cursor_date <= to_date):
            data[cursor_date.strftime('%Y-%m-%d')] = None
            cursor_date -= delta
    

    else:
        last_value = None
        for kts, value in zip(raw_data[-2::-2], raw_data[-1::-2]):
            if cursor_date < from_date:
                break

            ts = parse_keepa_ts(kts)
            last_value = value

            while (ts <= cursor_date) and (cursor_date >= from_date):

                # last_value 存在一种特殊情况，即为 -1 的时候，用来标记缺失
                data[cursor_date.strftime('%Y-%m-%d')] = last_value if last_value != -1 else None
                cursor_date -= delta



        while cursor_date >= from_date:
            data[cursor_date.strftime('%Y-%m-%d')] = None
            cursor_date -= delta
        
    return data

def parse_keepa_ts(keepa_ts):
    """
    keepa日期转换
    """
    keepa_start_minute = 21564000
    ts = keepa_ts * 60 + keepa_start_minute * 60
    return datetime.fromtimestamp(ts)

class SalesEstimator(object):
    def __init__(self):
        self.asin_pool = {}
        self.jsdata = jungle_scout_data
        self.category_count = self.jsdata['Category'].value_counts()
        self.__category_names = self.category_count.index
        self.dict_category_names = {category_name:self.get_most_similar_category_name(category_name) 
                                   for category_name in self.__category_names}
        if 'N.A.' in self.dict_category_names.keys():
            self.dict_category_names['N.A.'] = self.dict_category_names[self.jsdata['Category'].value_counts().drop('N.A.').index[0]]
        if configs.category_lock:
            for key in self.dict_category_names.keys():
                self.dict_category_names[key] = self.dict_category_names[self.jsdata['Category'].value_counts().drop('N.A.').index[0]]
        
    @property
    def list_depts(self):
        if configs.region == '德国':
            with open('{}/data_or_source/amz_de_list_dept.json'.format('.'), 'r') as f:
                return json.load(f)
        elif configs.region == '美国':
            with open('{}/data_or_source/amz_us_list_dept.json'.format('.'), 'r') as f:
                return json.load(f)
        elif configs.region == '英国':
            with open('{}/data_or_source/amz_uk_list_dept.json'.format('.'), 'r') as f:
                return json.load(f)
        else:
            raise BaseException("所选国家不是德国或者美国")
    
    @property
    def category_list(self):
        return [category['name'] for category in self.list_depts]
    
    def get_most_similar_category_name(self, category_name):
        d = {_category_name:SequenceMatcher(None,_category_name, category_name).ratio() for _category_name in self.category_list}
        return max(d, key=d.get)
        
    def estimate(self, asin, sales_rank):
        if asin in self.asin_pool:
            dept_name = self.asin_pool[asin]
        else:
            try:
                category = self.jsdata[self.jsdata['ASIN']==asin]['Category'].values[0]
            except:
                category = self.dict_category_names[self.jsdata['Category'].value_counts().drop('N.A.').index[0]]
            dept_name = self.dict_category_names[category]
            self.asin_pool[asin] = dept_name
        
        if sales_rank:
            if np.isnan(sales_rank):
                return np.nan
            sales_rank = float(sales_rank)
            sales_rank_of_log10 = math.log10(sales_rank)
            
            for dept in self.list_depts:
                if dept_name == dept['name']:
                    max_rank = dept['meta']['max_rank']
                    if int(sales_rank) > max_rank:
                        return 1

                    rankt = dept['meta']['rankt']
                    if int(sales_rank) <= rankt:
                        coef21 = dept['meta']['coef21']
                        coef22 = dept['meta']['coef22']
                        coef23 = dept['meta']['coef23']
                        intercept2 = dept['meta']['intercept2']

                        estimated_sales_para = (sales_rank * coef21 +
                                                sales_rank_of_log10 * coef22 +
                                                sales_rank_of_log10 * sales_rank * coef23 +
                                                intercept2)

                        estimated_sales = 10 ** estimated_sales_para

                        return int(round(estimated_sales))

                    coef1 = dept['meta']['coef1']
                    coef2 = dept['meta']['coef2']
                    coef3 = dept['meta']['coef3']
                    coef4 = dept['meta']['coef4']
                    coef5 = dept['meta']['coef5']
                    coef6 = dept['meta']['coef6']
                    coef7 = dept['meta']['coef7']
                    coef8 = dept['meta']['coef8']
                    coef9 = dept['meta']['coef9']
                    intercept = dept['meta']['intercept']

                    estimated_sales_para = (sales_rank * coef1 +
                                            sales_rank_of_log10 * coef2 +
                                            sales_rank_of_log10 ** 2 * coef3 +
                                            sales_rank_of_log10 ** 3 * coef4 +
                                            sales_rank_of_log10 ** 4 * coef5 +
                                            sales_rank_of_log10 ** 5 * coef6 +
                                            sales_rank_of_log10 ** 6 * coef7 +
                                            sales_rank_of_log10 ** 7 * coef8 +
                                            sales_rank_of_log10 ** 8 * coef9 +
                                            intercept)

                    estimated_sales = 10 ** estimated_sales_para
                    estimated_sales = int(round(estimated_sales))
                    if estimated_sales:
                        return estimated_sales
                    else:
                        logging.warning("can't estimated_sales by bsr")

sales_estimator = SalesEstimator()