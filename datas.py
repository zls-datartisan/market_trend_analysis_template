# -*- coding : utf-8 -*-
import os, re
import pandas as pd
from configs import *

def get_jungle_scout_data():
    ospath = './data_or_source'
    re_match_content = 'Amazon Best Sellers'
    data =  pd.read_csv('{}/data_or_source/Amazon Best Sellers.csv'
                                .format(folder_path),header=7)
    for filename in os.listdir(ospath):
        if filename == 'Amazon Best Sellers.csv':
            continue
        if re.match(re_match_content,filename):
            data_temp = pd.read_csv('{}/data_or_source/{}'
                                    .format(folder_path,filename),header=7)
            data = data.append(data_temp)
    data = data.drop_duplicates(['ASIN'])
    data['#'] = range(1, len(data)+1)
    data.index = data['#']
    data.to_csv(jungle_scout_data_path, index=False)

jungle_scout_data_path = '{}/data_or_source/{}.csv'.format(folder_path,category_name)
if not os.path.isfile(jungle_scout_data_path):
    get_jungle_scout_data()

jungle_scout_data = pd.read_csv(jungle_scout_data_path,header=0)
jungle_scout_data = jungle_scout_data[jungle_scout_data['Product Name']!='N.A.']