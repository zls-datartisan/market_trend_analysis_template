# -*- coding: utf-8 -*-

# 品类名
category_name = u"Women's Sandals" 
# 品类中文名
category_name_chinese = u'女士凉鞋' 

# 大品类中文名
category_main_chinese = {"Tools & Home Improvement" : "家装工具",
"Industrial & Scientific" : "工业科技",
"Office Products" : "办公用品",
"Automotive" : "汽车用品",
"Baby" : "母婴用品",
"Sports & Outdoors" : "户外运动",
"Home & Kitchen" : "家居用品",
"Clothing, Shoes & Jewelry" : "鞋服珠宝",
'Cell Phones & Accessories' : "手机及其配件"
}

# 品类目录
category_catalogue = u"""Clothing, Shoes & Jewelry > Women > Shoes > Sandals""" 
# 报告数
report_number = u'十一'
# 区域
region = '美国'
# 货币符号
if region[0] == '德':
    currency_symbol = '€'
if region[0] == '英':
    currency_symbol = '£'
elif region[0] == '美':
    currency_symbol = '$'

# 工作目录路径（用于调整主程序和工作目录不在同一位置的问题）
folder_path = '.' # './amz-market-trend-analysis-{}-{}'.format(category_name.replace(' ','-').replace('-&-','&'),date.today().strftime('%Y%m%d'))
category_lock = False # 是否锁定品类为大多数商品所属的品类

