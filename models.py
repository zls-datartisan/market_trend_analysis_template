# -*- coding: utf-8 -*-

from tools import *
from jinja2 import Template
import configs
import logging

def del_newline(instance):
    for key, value in instance.__dict__.items():
        if isinstance(value, str):
            instance.__dict__[key] = value.replace('\n', '').replace('$', configs.currency_symbol).replace('€', configs.currency_symbol)
        if isinstance(value, list):
            for index, subinstance in enumerate(instance.__dict__[key]):
                if isinstance(subinstance, Chapter):
                    instance.__dict__[key][index] = del_newline(subinstance)
        if isinstance(value, Chapter):
            instance.__dict__[key] = del_newline(value)
    return instance

class Chapter(object):
    def __init__(self, subchapter_number=0, number=0):
        self.title = ''
        self.foreword = ''
        self.number = str(number)
        self.tables = []
        self.images = []
        self.image_path = './image'
        self.content = ''

        self.subchapters = []
        for i in range(subchapter_number):
            subcapter = Chapter(number=self.number+str(i))
            self.subchapters.append(subcapter)

    def set_image(self, fig):
        image_filename = '{}/{}-0.png'.format(self.image_path,
            self.number)
        fig.savefig(image_filename, dpi=160, bbox_inches='tight')
        self.images = [image_filename]
    def add_image(self, fig):
        image_number = len(self.images)
        image_filename = '{}/{}-{}.png'.format(self.image_path,
            self.number,
            image_number)
        fig.savefig(image_filename, dpi=160, bbox_inches='tight')
        self.images.append(image_filename)

    def add_chapter(self, subchapter_number=1):
        """
        subchapter: the number of subchapter which will be added
        """
        self.subchapters.append(Chapter(subchapter_number=subchapter_number, 
            number=self.number+str(len(self.subchapters))))


class SubChapter(Chapter):
    pass

class Document(object):
    def __init__(self):
        self.title = "专题{}：{}{}".format(configs.report_number,
            configs.category_name_chinese,
            configs.category_name)

        self.subtitle = "{}亚{}品类趋势报告".format(configs.region[0], configs.category_main_chinese[self.category_main])
        self.foreword = '''作为亚马逊{}站{}品类榜单中的常见子品类之一，
        亚马逊{}站{}({})品类市场又有着怎样的特质与趋势呢？本篇报告为您解读。
        '''.format(configs.region[0], self.category_main, configs.region[0], configs.category_name_chinese,
            configs.category_name)
        self.category_catalogue = configs.category_catalogue
        self.sales_sum = ''
        self.sales_month = ''
        self.business_opportunity_evaluation = ''

        self.chapters = []
        for i in range(4):
            chapter = Chapter(subchapter_number=5-i, number=i)
            self.chapters.append(chapter)
        self.chapters.append(Chapter(subchapter_number=2))

    @property
    def category_main(self):
        return configs.category_catalogue.replace('\t', '').replace('\n', '').split(' >')[0]

    def save_html(self, html_name):
        document = del_newline(self)
        with open('./template.html') as f:
            templ = f.read()
        t = Template(templ)
        html = t.render(document=document)
        with open(html_name,'w') as f:
            f.write(html)

class Product(object):
    info_name = {1:'price',3:'bsr',16:'rating',17:'review_count'}
    def __init__(self, data):
        self.data = data
        self.asin = data['asin']
        self.bsr_data = self.data['csv'][3]
        self.price_data = self.data['csv'][1]
        self.rating_data = self.data['csv'][16]
        self.review_count = self.data['csv'][17]
        self.brand = self.data['brand']
    
    @property
    def last_bsr(self):
        if self.bsr_data:
            if len(self.bsr_data)>2:
                return list(filter(lambda x : x>0 and x<1000000, self.bsr_data))[-1]
        return None

    @property
    def seller(self):
        if self.data['stats']:
            if self.data['stats']['buyBoxIsFBA']:
                return 'FBA'
            if self.data['stats']['buyBoxIsAmazon']:
                return 'AMZ'
        return 'N.A.'

    def check_data(self):
        for i in info_name.keys():
            if (not self.data['csv'][i]) or self.data['csv'][i].__len__()<5: # 如果该部分内容的数据缺失
                logging.info('{} need info {} from parents {}'.format(self.asin,info_name[i],self.parents.asin))

    @property
    def parents(self):
        if self.data['variationCSV']:
            self.asin_parents = self.data['variationCSV'][0:10]
            if not self.asin_parents == self.asin:
                if isinstance(asin_parents, str):
                    try:                    
                        product_parents = fetch_data(self.asin_parents)
                        return Product(product_parents)
                    except:
                        logging.warning('Fail to fetch data from Parents : {}'.asin_parents)
                        return None
                else:
                    logging.warning('Type Error : asin_parents is not a string')
                    return None