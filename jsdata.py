# -*- coding : utf-8 -*-

import pandas as pd
import numpy as np
import re
from configs import *

product_info = pd.read_csv('../trend_indicator_statistic/jungle_statistic.csv',index_col=0)


def str2num(string):
    string = str(string)
    string = string.replace(',','')
    regular_expression_num = '\d+\.?\d*'
    pattern = re.compile(regular_expression_num)
    match = pattern.search(string)
    if match:
        return float(match.group())
    else:
        return float('nan')
        
class JSData(object):
    """The data from Jungle Scout"""
    
    def __init__(self, category_name):
        self.category_name = category_name
        self.__data = self.read_data()
        self.data = self.__data.drop_duplicates(['ASIN'])
        self.product_count = len(self.data)
        self.brand = self.Brand(self.sort_out_brand)
        self.review = self.Review(np.array(self.data['Reviews']))
        self.rating = self.Rating(np.array(self.data['Rating']))
        self.price = self.Price(np.array(self.data['Price']))
        self.sales = self.Sales(self.data['Sales'])
        self.bb_seller = self.BB_Seller(self.data['Seller'])
        
    class Price(object):
        result = {'price_span': [u'很小', u'较小', u'较大', u'很大']}
        def __init__(self, data):
            self.data = data[~np.isnan(data)]
            self.mean = self.data.mean()
            self.min = self.data.min()
            self.max = self.data.max()
            self.span = self.max - self.min
            self.span_verdict = self.value_verdict(self.span, value_name = 'price_span')
        
        def value_verdict(self, value, proportion=25, value_name = 'price_span'):
            global product_info
            
            def get_span(series):
                return [np.percentile(series,i) for i in [proportion,50,100-proportion]]
            def discrimination_result(constant,discrimination):
                for i in range(3):
                    if constant < discrimination[i]:
                        return self.result[value_name][i]
                return self.result[value_name][3]
            
            product_info.loc[category_name, value_name] = value
            span_discrimination = get_span(product_info.loc[:, value_name])
            return discrimination_result(value, span_discrimination)
        
    class Review(Price):
        result = {'review_meaning': [u'很少', u'较少', u'较多', u'很多'],
                 'competitive_pressure':[u'很小', u'较小', u'较大', u'很大']}
        def __init__(self, data):
            self.data = data[~np.isnan(data)]
            self.mean = self.data.mean()
            self.min = int(self.data.min())
            self.max = int(self.data.max())
            self.mean_verdict = self.value_verdict(self.mean, value_name = 'review_meaning')
            self.added_text = self.add_text()
        def add_text(self):
            hist,bin_edges = np.histogram(self.data, bins=[0,100,500,2500,100000])
            hist = hist*100/hist.sum()#转换成百分比
            if self.mean_verdict in self.result['review_meaning'][2:]:
                added_text = u"有{}%的商品评论在100条{}，{}%的商品评论数在500条以上，".format(hist[1:].sum().__int__(),u'以上',hist[2:].sum().__int__())
            else:
                added_text = u"有{}%的商品评论在100条{}，{}%的商品评论数在500条以上，".format(hist[0].sum().__int__(),u'以下',hist[2:].sum().__int__())
            competitive_pressure_index = self.result['review_meaning'].index(self.mean_verdict)
            self.bestsellers = ''
            if hist[3] == 0:
                self.bestsellers = u'没有评论量在2500以上的商品，'
                if competitive_pressure_index != 0:
                    competitive_pressure_index -= 1
            else:
                self.bestsellers = u'有{}款评论量在2500以上的商品，'.format(hist[3].__int__())
            self.competitive_pressure = self.result['competitive_pressure'][competitive_pressure_index]            
            return added_text
            
    class Rating(Price):
        result = {'rating': [u'很低', u'较低', u'较高', u'很高'],
                 'improved_space': [u'很大', u'较大', u'较小', u'很小']}
        def __init__(self, data):
            self.data = data[~np.isnan(data)]
            self.mean = self.data.mean()
            self.min = self.data.min()
            self.max = self.data.max()
            self.mean_verdict = self.value_verdict(self.mean, value_name = 'rating')
            self.improved_space = self.result['improved_space'][self.result['rating'].index(self.mean_verdict)]
            self.added_text = self.add_text()
            
        def add_text(self):
            rating_range = [u'3星以下',u'3.01-4星',u'4.01-4.5星',u'4.51-5星']
            hist,bin_edges = np.histogram(self.data, bins=[0,3.01,4.01,4.51,5.01])
            hist = (hist*100)/hist.sum()
            hist_argsort = hist.argsort()
            added_text = u'其中评论星级分布在{}的商品数最多，占比{}%，有{}%的商品评论星级在{}。'.format(rating_range[hist_argsort[-1]],hist[hist_argsort[-1]].__int__(),hist[hist_argsort[-2]].__int__(),rating_range[hist_argsort[-2]])
            added_text += u'品类平均评论星级{}，产品改进空间{}。'.format(self.mean_verdict,self.improved_space)
            return added_text

    class Sales(Price):
        result = {'sales_sum': [u'很低', u'较低', u'较高', u'很高'],
                 'top_10_sales_rate': [u'非常分散', u'较为分散', u'较为集中', u'非常集中']}
        def __init__(self, data):
            self.data = data
            self.sum = float(self.data.sum())
            self.sum_verdict = self.value_verdict(self.sum, value_name = 'sales_sum')
            self.top_10_sales_rate = self.data[0:10].sum()/self.sum
            self.top_10_percentage = "%.2f"%(self.top_10_sales_rate*100)
            self.concentration_verdict = self.value_verdict(self.top_10_sales_rate, value_name = 'top_10_sales_rate')
            if self.sum_verdict == u'很高' and self.concentration_verdict == u'非常分散':
                self.review = u'，排名靠后的商品同样有销售机会'
            else:
                self.review = ''
    class Brand(Price):
        result = {'brand_count': [u'很少', u'较少', u'较多', u'很多'],
        'competitive_pressure': [u'很小', u'较小', u'较大', u'很大']}
        def __init__(self, data):
            self.data = data
            self.brand_info = self.data.head()
            self.count = self.data.__len__()
            self.count_verdict = self.value_verdict(self.count, value_name = 'brand_count')
            self.review = self.review_brand()

        def review_brand(self):
            def evaluate_advantage(sales):
                if sales > 20:
                    return u'很大'
                elif sales > 10:
                    return u'较大'
                else:
                    return u'很小'
            df_merged = self.data
            brand = [{}, {}]
            self.review_added = ''
            brand[0]['name'] = df_merged.Brand.values[1]
            brand[0]['count'] = int(df_merged.Count.values[1])
            brand[0]['sales'] = df_merged.Sales.values[1]*100
            brand[0]['evaluate_advantage'] = evaluate_advantage(brand[0]['sales'])
            brand_review = u'其中{}表现最好，有{:d}款商品进入榜单，销量占总销量的{:.2f}%，对其他品牌有{}优势，'.format(brand[0]['name'],brand[0]['count'],brand[0]['sales'],brand[0]['evaluate_advantage'])
            brand[1]['name'] = df_merged.Brand.values[2]
            brand[1]['count'] = int(df_merged.Count.values[2])
            brand[1]['sales'] = df_merged.Sales.values[2]*100
            brand[1]['evaluate_advantage'] = evaluate_advantage(brand[1]['sales'])
            if brand[0]['sales']<10:
                self.competitive_pressure_index = 0
            elif brand[0]['sales']<20:
                if brand[1]['sales']>10:
                    self.review_added = u'{}同样表现较好，有{:d}款商品进入榜单，销量占总销量的{:.2f}%，对其他品牌有{}优势，'.format(brand[1]['name'],brand[1]['count'],brand[1]['sales'],brand[1]['evaluate_advantage'])
                    if brand[0]['sales']+brand[1]['sales']>30:
                        self.competitive_pressure_index = 3
                    else:
                        self.competitive_pressure_index = 2
                else:
                    if brand[0]['sales'] + brand[1]['sales']>20:
                        self.competitive_pressure_index = 1
                    else:
                        self.competitive_pressure_index = 0
            else:
                if brand[1]['sales']>10:
                    self.review_added = u'{}同样表现较好，有{:d}款商品进入榜单，销量占总销量的{:.2f}%，对其他品牌有{}优势，'.format(brand[1]['name'],brand[1]['count'],brand[1]['sales'],brand[1]['evaluate_advantage'])
                self.competitive_pressure_index = 3
            if self.count_verdict == u'很少' and self.competitive_pressure_index != 0:
                self.competitive_pressure_index -= 1
            self.competitive_pressure = self.result['competitive_pressure'][self.competitive_pressure_index]

            return brand_review
    
    class BB_Seller(object):

        def __init__(self, data):
            self.data = data[data != 'N.A.']
            value_counted = self.data.value_counts()
            self.value_counted = value_counted*100/value_counted.sum()
            self.added_text = self.add_text()

        def add_text(self):
            value_counted = self.value_counted
            if value_counted.index[0] == 'FBA':
                if value_counted[0]>=70:
                    self.seller_friendliness = u"对跨境卖家非常友好"
                elif value_counted[0]>=50:
                    self.seller_friendliness = u"对跨境卖家较为友好"
                else:
                    self.seller_friendliness = u'FBA卖家占比最多但不足半'
            elif value_counted.index[0] == 'AMZ':
                if value_counted[0]>=50:
                    self.seller_friendliness = u"AMZ卖家过半"
                else:
                    self.seller_friendliness = u'AMZ卖家占比最多但不足半'
            else:
                logging.warning('FBM卖家最多')
            added_text = u"商品有{:.0f}%来自于{}，{:.0f}%来自于{}，{}。".format(value_counted[0],value_counted.index[0],value_counted[1],value_counted.index[1],self.seller_friendliness)
            return added_text
        
    def fill_brand(self):
        self.brand_filled = []
        brand_set = set(self.data['Brand'])
        df_without_brand = self.data[self.data['Brand']=='N.A.']
        for i in df_without_brand.index:
            product_name = self.data['Name'][i]
            possible_brand_name = product_name.split(' ')[0]
            if not (possible_brand_name in brand_set):
                self.data.loc[i,'Brand'] = possible_brand_name
                self.brand_filled.append(possible_brand_name)
            # Todo: 为当品牌前二有额外添加品牌时，加入报警

    @property
    def sort_out_brand (self):
        self.fill_brand()
        brand_count = self.data.pivot_table(values=['Rank'],index='Brand',aggfunc='count',margins=True)
        category_pivot = self.data.pivot_table(values=['Sales'],index='Brand',aggfunc='sum',margins=True)
        df_merged = category_pivot.merge(brand_count,on=category_pivot.index.values)
        df_merged.columns = ['Brand','Sales','Count']
        df_merged.loc[:,'Sales'] = (df_merged['Sales']/df_merged['Sales'].max())
        return df_merged.sort_values('Sales',ascending=False)     

    
    @staticmethod
    def read_data(header=0):
        # dtype = {'#':str,
        # 'Name':str,
        # 'Brand':str,
        # 'Price':str,
        # 'Category':str,
        # 'Rank':str,
        # 'Sales':str,
        # 'Revenue':str,
        # 'Reviews':str,
        # 'Rating':str,
        # 'Seller':str,
        # 'ASIN':str}
        converters = {'Price':str2num,
                      'Rank':str2num,
                      'Rating':str2num,
                      'Sales':str2num,
                     'Revenue':str2num,
                     'Reviews':str2num}
        data = pd.read_csv('{}/data_or_source/{}.csv'.format(folder_path,category_name),
                          converters=converters,
                          header=header,
                          encoding='latin-1')
        return data 
    
    def __call__(self):
        self.data.to_csv('{}/{}.csv'.format(folder_path,category_name),index=False)
        product_info.to_csv('../trend_indicator_statistic/jungle_statistic.csv')
        print(u'去重后商品数为：{}'.format(self.data.__len__()))

jsdata = JSData(category_name)